# Le Cookie Libre

Basé sur Hugobricks

## Mise en place

- Créer le dépôt git avec un README sur Github / Gitlab
- Le clone en local : `git clone <YOUR_REPO>` (a ce point tu dois avoir une branch main)
- Créer une nouvelle branch "hugobricks" et checkout vers cette branche : `git checkout --orphan hugobricks & git rm --cache README.md`
- Ajouter la remote vers https://github.com/jhvanderschee/hugobricks : `git remote add hugobricks https://github.com/jhvanderschee/hugobricks`
- Pull : `git pull hugobricks main`
- Push : `git push origin hugobricks`
- Checkout vers le main/master
- `git checkout hugobricks -- themes` https://stackoverflow.com/questions/4479960/git-checkout-to-a-specific-folder
- Push vers ton main : `git push origin main`

## Pour mettre à jour le thème

- Aller sur la branche hugobricks : `git checkout hugobricks`
- Puller les nouvelles dernières modifications depuis le dépôt originel : `git pull hugobricks main`
- Pusher les nouvelles modifs sur notre dépot : `git push origin hugobricks`
- Rebasculer sur la branche main de notre dépôt : `git checkout main`
- Faire la synchronisation du répertoire theme : `git checkout hugobricks -- themes`
- Pousser vers ton main : `git push origin main`
